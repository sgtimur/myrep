package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int[] dimensions = getPyramidDimensions(inputNumbers.size());
        int pyramidHeight = dimensions[0];
        if (pyramidHeight == 0) throw new CannotBuildPyramidException();
        int pyramidWidth = dimensions[1];

        try {
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError error) {
            throw new CannotBuildPyramidException();
        }

        int pyramid[][] = new int[pyramidHeight][pyramidWidth];
        int currentNumberIndex = 0;

        for (int i = 0; i < pyramidHeight; i++) {
            for (int j = 0; j < (pyramidWidth - i) / 2; j++) {
                pyramid[i][j] = 0;
            }

            for (int j = (pyramidWidth) / 2 - i; j < (pyramidWidth - (pyramidWidth / 2 - i)); ) {
                if (j < pyramidWidth) {
                    pyramid[i][j++] = inputNumbers.get(currentNumberIndex++);
                }
                if (j < pyramidWidth) {
                    pyramid[i][j++] = 0;
                }
            }

            for (int j = (pyramidWidth - (pyramidWidth / 2 - i)); j < pyramidWidth; j++) {
                pyramid[i][j] = 0;
            }
        }

        return pyramid;
    }

    private int[] getPyramidDimensions(int numbersListSize) {
        int currentSize = 1;
        int res = 0;
        int height = 0;
        while (res <= numbersListSize) {
            res += currentSize;
            height++;
            if (res == numbersListSize) {
                return new int[]{height, 2 * currentSize - 1};
            }
            currentSize++;
        }

        return new int[]{0};
    }
}
