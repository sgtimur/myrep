package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x==null||y==null) throw new IllegalArgumentException();
        if (x.size()>y.size()) { return false; }

        int i = 0;
        for (int j = 0; i<x.size()&&j<y.size(); j++) {
            if (x.get(i).equals(y.get(j))) {
                i++;
            }
        }

        return i==x.size();
    }

    public static void main(String[] args) {
        List x = Stream.of(1,3,2,5).collect(Collectors.toList());
        List y = Stream.of(0,1,3,5,2,5).collect(Collectors.toList());
        System.out.println(new Subsequence().find(x,y));

    }
}
